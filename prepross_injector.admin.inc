<?php
// $Id$ 

/**
 * @file
 * Administrative page callbacks for the Prepross Injector module.
 */
 
/**
 * Inclusion of class PreprossInjector to handle preprocessing injection of files
 */ 
include_once("classes/PreprossInjector.class.php");

/**
 * Return a listing of all defined node settings.
 * When filter key passed, perform a standard search on the given key,
 * and return the list of matching files related to his node setting.
 */	
function prepross_injector_admin_settings($keys = null){
	$sql = "select count(*) as count from {prepross_injector_node}";
	$count = db_fetch_object(db_query($sql, $keys));
	$output = '';
	if ($count->count == 0){
		drupal_set_message('You haven\'t created any node setting yet. <a href="'.base_path().'admin/settings/prepross_injector/add">Create a new node setting</a>', 'warning');
	}elseif(!$keys){
			$output .= drupal_get_form('prepross_injector_id_filter_form', $keys);
			$output .= drupal_get_form('prepross_injector_list_node_configuration_form');
	}else{
			$output .= drupal_get_form('prepross_injector_list_files_form', $keys);
	}
	return $output;
}

/**
 * Return a form for creating an individual node setting.
 *
 * @ingroup forms
 * @see prepross_injector_add_setting_form_submit()
 */
function prepross_injector_add_setting_form(&$form_state, $keys = ''){	
	$form['node_id'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Node ID'), 
	  '#size' => 25, 
	  '#default_value'=> $keys,
	  '#maxlength' => 256
	);
	$form['submit']=array(
	  '#type'=>'submit',
	  '#value'=>t('add'), 
	);		
	return ($form);
}	

/**
 * Check node id setting.
 */
function prepross_injector_add_setting_form_validate($form, &$form_state){
	$nid = trim($form_state['values']['node_id']);
	$sql = "select nid from {prepross_injector_node} where nid = %d";
	$result = db_fetch_object(db_query($sql, $nid));
	if(empty($nid)){
		form_set_error('node_id', 'Node ID cannot be empty.', 'error');
	}
	if($result && !empty($nid)){
		form_set_error('node_id', 'Node setting already exist.', 'error');
	}
}

/**
 * Save a new node setting to the database.
 */	
function prepross_injector_add_setting_form_submit($form, &$form_state){	
	$nid = $form_state['values']['node_id'];
	if	(node_load($nid)){
		$pi = PreprossInjector::getInstance();
		$vars['node']->nid = $nid;
		$pi->init($vars);
		drupal_set_message("Your setting for the node/".$nid." as been created.");
		drupal_set_message("You have been redirected to the node setting.");		
		$form_state['redirect'] = 'admin/settings/prepross_injector/list/'. $nid;
	}else{
		drupal_set_message("The node/".$nid." doesn't exist.", "error");
		drupal_set_message("You can't add a configuration to an unexisting node.", "warning");
	}
}	
 
/**
 * Return a form for edit or delete a node setting.
 */ 
function prepross_injector_list_node_configuration_form(&$form_state){
		$sql = "select nid from {prepross_injector_node} group by nid";
		$header = PreprossInjector::getHeader("theme_prepross_injector_list_node_configuration_form");
		$sql .= tablesort_sql($header);
		$result = db_query($sql);
		$i = 0;
		while ($obj = db_fetch_object($result)){
			$i++;
			$row = array();
			$row["node"] = 'node/'.$obj->nid;
			$row["edit"] = "<a href='".base_path()."admin/settings/prepross_injector/list/".$obj->nid."'>Edit</a>";
			$row["delete"] = "<a href='".base_path()."admin/settings/prepross_injector/delete/".$obj->nid."''>Delete</a>";
			$form['rows'][$i]['data-'.$i] = array('#type' => 'value',
						   '#value' => $row);						
		}
		return ($form);
}

/**
 * Return a listing of all node setting.
 */	
function theme_prepross_injector_list_node_configuration_form($form){
	if ($form['rows']){
		foreach($form['rows'] as $id => $row){
			if (intval($id)){
				$this_row = array();
				$this_row[] = $row['data-'.$id]['#value']["node"];
				$this_row[] = $row['data-'.$id]['#value']["edit"];
				$this_row[] = $row['data-'.$id]['#value']["delete"];
				$table_rows[] = array('data'=>$this_row, 'class' => 'draggable');
			}
		}
	}
	$header = array(
		array('data' => t('Node'), 'field' => 'nid', 'sort' => 'asc'),
	);
	$output .= theme('table', $header, $table_rows, array('id' => 'prepross_injector-list-node-configuration'));
	$output .= drupal_render($form);
	//drupal_add_tabledrag('prepross_injector-list-node-configuration', 'order', 'sibling', 'node');		
    return $output;
}
	
/**
 * Return a form for to filter node id setting.
 */ 
function prepross_injector_id_filter_form(&$form_state, $keys = ''){
	$form['filter'] = array(
	  '#type' => 'textfield', 
	  '#title' => t('Node ID'), 
	  '#size' => 25, 
	  '#default_value'=> $keys,	  
	  '#maxlength' => 256
	);
	$form['submit']=array(
	  '#type'=>'submit',	  
	  '#value'=>t('filter'), 
	);		
	return ($form);
}

/**
 * Check node id setting.
 */
function prepross_injector_id_filter_form_validate($form, &$form_state){
	$nid = trim($form_state['values']['filter']);
	$sql = "select nid from {prepross_injector_node} where nid = %d";
	$result = db_fetch_object(db_query($sql, $nid));
	if(empty($nid)){
		 form_set_error('filter',"Cannot be empty.", "error");
	}
	if(!$result){
		 form_set_error('filter', "No existing configuration for node id ".$nid, "error");
	}
}
	
/**
 * Redirect to a listing of all related files to the node id setting.
 */	
function prepross_injector_id_filter_form_submit($form, &$form_state){
	$nid = trim($form_state['values']['filter']);
	$form_state['redirect'] = 'admin/settings/prepross_injector/list/'. $nid;
}

/**
 * Return a form for edit files in the node setting.
 */ 
function prepross_injector_list_files_form(&$form_state, $keys = null){
	if(isset($keys)){
		$pi = PreprossInjector::getInstance();
		$pi->setNid($keys);
		$pi->find();
		
		$status = $pi->getCss();
		$i = 1;
		if($status){
			
			foreach ($status as $stat){
				foreach($stat as $obj){
						
						switch ($obj->getStatus()) {
						case "include":
							$status = array('checked' => 'checked');
							break;
						case "exclude":
							$status = '';
							break;
						}
						$form['css']['rows'][$i]['include-'.$i] = array(
							'#type' => 'checkbox', 
							'#attributes' => $status,
						);
						$form['css']['rows'][$i]['weight-'.$i] = array(
							'#type' => 'textfield', 
							'#size'=> 5,
							'#default_value'=> $obj->getWeight(),
							'#attributes' => array('class'=>'weight'),
						);
						$row = array();
						$row["path"] = $obj->getPath();
						$row["type"] = $obj->getType();
						$row["media"] = $obj->getMedia();
						$row["preprocess"] = $obj->getPreprocess();
						$form['css']['rows'][$i]['data-'.$i] = array('#type' => 'value',
									   '#value' => $row);
						$i++;
				}
			}
		}
		

		$status = $pi->getJs();
		if($status){
			foreach ($status as $stat){
				foreach($stat as $obj){
						switch ($obj->getStatus()) {
						case "include":
							$status = array('checked' => 'checked');
							break;
						case "exclude":
							$status = '';
							break;
						}
						$form['js']['rows'][$i]['include-'.$i] = array(
							'#type' => 'checkbox', 
							'#attributes' => $status,
						);
						$form['js']['rows'][$i]['weight-'.$i] = array(
							'#type' => 'textfield', 
							'#size'=> 5,
							'#default_value'=> $obj->getWeight(),
							'#attributes' => array('class'=>'weight'),
						);
						$row = array();
						$row["path"] = $obj->getPath();
						$row["type"] = $obj->getType();
						$row["scope"] = $obj->getScope();
						$row["defer"] = $obj->getDefer();
						$row["cache"] = $obj->getCache();
						$row["preprocess"] = $obj->getPreprocess();
						$form['js']['rows'][$i]['data-'.$i] = array('#type' => 'value',
									   '#value' => $row);
						$i++;
				}
			}	
		}		
	}
	$form['js']['rows']['submit']=array(
	  '#type' => 'submit',
	  '#value' => t('Save setting'),
	);
	return ($form);
}	
	
/**
 * Return a listing of all related files to the node id setting.
 */ 
function theme_prepross_injector_list_files_form($form){
	if ($form['css']['rows']){
		$output .= '<div class="pi-category">CSS</div>';
		$table_rows = array();
		foreach($form['css']['rows'] as $id => $row){
			if (intval($id)){  
				$this_row[0] = drupal_render($form['css']['rows'][$id]['include-'.$id]);
				$this_row[1] = $row['data-'.$id]['#value']["path"];
				$this_row[2] = $row['data-'.$id]['#value']["type"];
				$this_row[3] = $row['data-'.$id]['#value']["media"];
				$this_row[4] = $row['data-'.$id]['#value']["preprocess"] ? 'true' : 'false';
				$this_row[5] = drupal_render($form['css']['rows'][$id]['weight-'.$id]);
				$table_rows[] = array('data'=>$this_row, 'class' => 'draggable');
			}
		}
		$header = array(
			array('data' => t('Include'), 'colspan' => 'include'),
			array('data' => t('Path'), 'colspan' => 'path'),
			array('data' => t('Type'), 'colspan' => 'type'),
			array('data' => t('Media'), 'colspan' => 'media'),
			array('data' => t('Preprocess'), 'colspan' => 'preprocess'),
			array('data' => t('Weight'), 'field' => 'weight', 'sort' => 'asc'),
		);	
		$output .= theme('table', $header, $table_rows, array('id' => 'prepross_injector-list-files-css'));
		drupal_add_tabledrag('prepross_injector-list-files-css', 'order', 'sibling', 'weight');
	}


	
	
	if ($form['js']['rows']){
		$output .= '<div class="pi-category">Javascript</div>';
		$table_rows = array();
		foreach($form['js']['rows'] as $id => $row){
			if (intval($id)){  
				$this_row[0] = drupal_render($form['js']['rows'][$id]['include-'.$id]);
				$this_row[1] = $row['data-'.$id]['#value']["path"];
				$this_row[2] = $row['data-'.$id]['#value']["type"];
				$this_row[3] = $row['data-'.$id]['#value']["scope"];
				$this_row[4] = $row['data-'.$id]['#value']["defer"] ? 'true' : 'false';
				$this_row[5] = $row['data-'.$id]['#value']["cache"] ? 'true' : 'false';
				$this_row[6] = $row['data-'.$id]['#value']["preprocess"] ? 'true' : 'false';
				$this_row[7] = drupal_render($form['js']['rows'][$id]['weight-'.$id]);
				$table_rows[] = array('data'=>$this_row, 'class' => 'draggable');
			}
		}
		$header = array(
			array('data' => t('Include'), 'colspan' => 'include'),
			array('data' => t('Path'), 'colspan' => 'path'),
			array('data' => t('Type'), 'colspan' => 'type'),
			array('data' => t('Scope'), 'colspan' => 'scope'),
			array('data' => t('Defer'), 'colspan' => 'defer'),
			array('data' => t('Cache'), 'colspan' => 'cache'),
			array('data' => t('Preprocess'), 'colspan' => 'preprocess'),
			array('data' => t('Weight'), 'field' => 'weight', 'sort' => 'asc'),
		);		
		$output .= theme('table', $header, $table_rows, array('id' => 'prepross_injector-list-files-js'));
		drupal_add_tabledrag('prepross_injector-list-files-js', 'order', 'sibling', 'weight');			
	}	

	$output .= drupal_render($form);
		
    return $output;
}	
 
/**
 * Update a node id setting.
 */
function prepross_injector_list_files_form_submit($form, &$form_state) {

	drupal_set_message("Setting has been saved.");	
	// save the css config	
	if($form['css']['rows']){
		foreach ($form['css']['rows'] as $id => $row){
			if (intval($id)){  
				$file 			= $form_state['values']['data-'.$id]['path'];
				$type 			= $form_state['values']['data-'.$id]['type'];
				$media 			= $form_state['values']['data-'.$id]['media'];
				$preprocess		= $form_state['values']['data-'.$id]['preprocess'];
				$status 		= $form_state['values']['include-'.$id] ? "include" : "exclude";
				$weight			= $form_state['values']['weight-'.$id];
				$obj = new CSS($file, $type, $media, $preprocess, $status, $weight);
				$pi = PreprossInjector::getInstance();
				$pi->setNid(arg(4));
				$pi->update($obj);		
			}
		}
	}else{
		drupal_set_message("No files available.", "warning");
		$form_state['redirect'] = 'admin/settings/prepross_injector';	
	}

	// save the js config
	if($form['js']['rows']){
		foreach ($form['js']['rows'] as $id => $row){
			if (intval($id)){  

				$file 			= $form_state['values']['data-'.$id]['path'];
				$type 			= $form_state['values']['data-'.$id]['type'];
				$scope 			= $form_state['values']['data-'.$id]['scope'];
				$defer 			= $form_state['values']['data-'.$id]['defer'];
				$cache 			= $form_state['values']['data-'.$id]['cache'];
				$preprocess		= $form_state['values']['data-'.$id]['preprocess'];
				$status 		= $form_state['values']['include-'.$id] ? "include" : "exclude";
				$weight			= $form_state['values']['weight-'.$id];
				$obj = new JS($file, $type, $scope, $defer, $cache, $preprocess, $status, $weight);
				//$export = dprint_r($obj , TRUE, $name, 'var_dump', FALSE);
				//drupal_set_message($export);
				
				$pi = PreprossInjector::getInstance();
				$pi->setNid(arg(4));
				$pi->update($obj);		
			}
		}
	}else{
		drupal_set_message("No files available.", "warning");
		$form_state['redirect'] = 'admin/settings/prepross_injector';	
	}
} 

/**
 * Menu callback; confirms deleting a node setting
 */
function prepross_injector_setting_delete_confirm(&$form_state, $nid) {
	$sql = "select nid from {prepross_injector_node} where nid = %d";
	$obj = db_fetch_object(db_query($sql, $nid));
    $form['nid'] = array('#type' => 'value', '#value' => $nid);
    $output = confirm_form($form,
      t('Are you sure you want to delete setting node/%title?', array('%title' => $obj->nid)), "admin/settings/prepross_injector");
	return $output;
}

/**
 * Execute a node setting deletion
 */
function prepross_injector_setting_delete_confirm_submit($form, &$form_state) {
  $id = $form_state['values']['nid'];
  if ($form_state['values']['confirm']) {
	$pi = PreprossInjector::getInstance();
	$pi->delete($id);	
	drupal_set_message("node/".$id." setting has been deleted");
	$form_state['redirect'] = "admin/settings/prepross_injector";
    return;
  }
}
