<?php
// $Id$ 
include_once("DAO.abstract.php");
include_once("CSS.class.php");
include_once("JS.class.php");

class PreprossInjector extends DAO
{
	private static $instance = null;
	private $css;
	private $js;
	private $nid;
	
	public static function getInstance($connect = null){
		if(!self::$instance){
			self::$instance = new PreprossInjector($connect);
		}
		return (self::$instance);	
	}
	
	function __construct($nid = null){
		$this->nid = $nid;
	}
	
    public function __clone()
    {
        trigger_error('Clone is not allowed.', E_USER_ERROR);
    }

	public function excludesCSStoArray(){
		if ($this->css['exclude']){
			foreach($this->css['exclude'] as $exclude){
				$excludes[] = $exclude->getPath();
			}
		}
		return ($excludes);
	}	

	public function excludesJStoArray(){
		if ($this->js['exclude']){
			foreach($this->js['exclude'] as $exclude){
				$excludes[] = $exclude->getPath();
			}
		}
		return ($excludes);
	}	
	
	private function buildConf(){
		$this->init_theme();
		
		$css = drupal_add_css();
		foreach ($css as $media => $types) {
			foreach ($types as $type => $files) {
				foreach($files as $path => $preprocess){
					$obj = new CSS($path, $type, $media, $preprocess);
					$this->create($obj);
					$tab['css'][] = $obj;
				}
			}
		}
		
		$js = drupal_add_js();
		foreach ($js as $type => $path) {
			if ($type != "setting"){
				foreach ($path as $file => $tab) {
					$obj = new JS($file, $type);
					$this->create($obj);
					$tab['js'][] = $obj;
				}
			}
		}
		return ($tab);
	}
	
	public function init($vars)
    {	
        $this->nid = $vars['node']->nid;
		$tab = $this->find();
		if((!$tab['css'] && $this->nid) || (!$tab['js'] && $this->nid)){
			$tab = $this->buildConf();
		}
		$this->css = $tab['css'];
		$this->js = $tab['js'];	
		return ($tab);
    }

	public function includeFiles(){
		$styles		= $this->getCss();
		$scripts	= $this->getJs();
	
		if($styles['include']){		
			foreach($styles['include'] as $style){
				drupal_add_css($style->getPath(), $style->getType(), $style->getMedia(), $style->getPreprocess());
			}
		}
		
		if($scripts['include']){		
			foreach($scripts['include'] as $script){
				drupal_add_js($script->getPath(), $script->getType());
			}
		}			
	}

	public function excludeFiles($type){
		$excludes['css']['func_name'] = "excludeCss";
		$excludes['js']['func_name'] = "excludeJs";
		
		if($excludes[$type])
			return ($this->$excludes[$type]['func_name']());
		else
			return (false);
	}
	
	private function excludeCss(){
		$this->init_theme();
		$css = drupal_add_css();
		//var_dump($css);
		$excludes 	= $this->excludesCSStoArray();
		
		$types[] 	= "module";
		$types[] 	= "theme";
		
		$medias[] 	= 'all';
		$medias[] 	= 'braille';
		$medias[] 	= 'embossed';
		$medias[] 	= 'handheld';
		$medias[] 	= 'print';
		$medias[] 	= 'projection';
		$medias[] 	= 'screen';
		$medias[] 	= 'speech';
		$medias[] 	= 'tty';
		$medias[] 	= 'tv';
		
		if ($excludes){
			foreach ($medias as $media){
				foreach ($types as $type){
					if (count($css[$media]) || count($css[$media][$type])){
						foreach ($css[$media][$type] as $k => $path) {
							$file = $k;
							if (in_array($file, $excludes)) {
								unset($css[$media][$type][$k]);
							}
						}
					}
				}
			}
		}
		return ($css);	
	}

	private function excludeJs(){
		$this->init_theme();
		$js = drupal_add_js();
		$excludes 	= $this->excludesJStoArray();
		
		$types[] 	= "core";
		$types[] 	= "module";
		$types[] 	= "theme";
		
		if ($excludes){
			foreach ($types as $type){
				if (count($js[$type])){
					foreach ($js[$type] as $k => $path) {
						$file = $k;
						if (in_array($file, $excludes)) {
							unset($js[$type][$k]);
						}
					}
				}
			}
		}
		return ($js);
	}
	
	public function handlePreprocess($vars){
		$this->init($vars);
		$this->includeFiles(); // must be performed before excludeFiles()
		$css_to_include = $this->excludeFiles('css');
		$js_to_include = $this->excludeFiles('js');
		$vars['styles'] = drupal_get_css($css_to_include);
		
		$vars['scripts'] = drupal_get_js('header', $js_to_include);
		//var_dump($vars['scripts']);
		return ($vars);
	}

	public function find($id = null){
		if(!$id)
			$id = $this->nid;
		
		$objs = array();
		$gender = 'css';
		$sql = "select * from {prepross_injector_node} where gender = '%s' and nid = %d"; 
		$header = $this->getHeader("theme_prepross_injector_list_files_form");
		$sql .= tablesort_sql($header);
		$result = db_query($sql, $gender, $id);
		while ($obj = db_fetch_object($result)) {
			$objs[$obj->status][] = new CSS(
				$obj->path, 
				$obj->type, 
				$obj->media, 
				$obj->preprocess,
				$obj->status,
				$obj->weight
			);
		}		
		$this->css = $objs;
		$tab[$gender] = $objs;

		$objs = array();
		$gender = 'js';
		$sql = "select * from {prepross_injector_node} where gender = '%s' and nid = %d"; 
		$header = $this->getHeader("theme_prepross_injector_list_files_form");
		$sql .= tablesort_sql($header);
		$result = db_query($sql, $gender, $id);
		while ($obj = db_fetch_object($result)) {
			
			$objs[$obj->status][] = new JS(
				$obj->path, 
				$obj->type, 
				$obj->scope, 
				$obj->defer, 
				$obj->cache, 
				$obj->preprocess,
				$obj->status,
				$obj->weight
			);
		}		
		$this->js = $objs;
		$tab[$gender] = $objs;
		return ($tab);
	}

	public function create($obj){
		
		$queries['css'] = $sql = "insert into {prepross_injector_node}	(
															nid,
															path, 
															type, 
															media, 
															preprocess, 
															status, 
															weight, 
															gender
														)    
												values	(
															'%d',
															'%s',
															'%s',
															'%s',
															'%d',
															'%s',
															'%d',
															'%s'
														)"; 
														
		$queries['js'] = $sql = "insert into {prepross_injector_node}	(
															nid,
															path, 
															type,
															scope,
															defer,
															cache,
															gender
														)    
												values	(
															'%d',
															'%s',
															'%s',
															'%s',
															'%d',
															'%d',
															'%s'
						
						)"; 	
						
		switch ($obj->getGender()){
		case "css" :
		db_query($queries[$obj->getGender()],
						$this->nid,
						$obj->getPath(),
						$obj->getType(),
						$obj->getMedia(),
						$obj->getPreprocess(),
						$obj->getStatus(),
						$obj->getWeight(),
						$obj->getGender()
				);
		break;
		case "js" :
		db_query($queries[$obj->getGender()],
						$this->nid,
						$obj->getPath(),
						$obj->getType(),
						$obj->getScope(),
						$obj->getDefer(),
						$obj->getCache(),
						$obj->getGender()
				);
			break;
		}
	}
	
	public function update($obj){
		$sql = "update {prepross_injector_node} set status = '%s', weight = %d where nid = %d and path = '%s'";
		db_query($sql, $obj->getStatus(), $obj->getWeight(), $this->nid, $obj->getPath());
		if(mysql_errno())
			return (false);
		return (true);
	}

	public function delete($id = null){
		if(!$id)
			$id = $this->id;
		$sql = "delete from {prepross_injector_node} where nid = %d";
		db_query($sql, $id);
	}	

	public static function getHeader($name_header){
		$headers["theme_prepross_injector_list_files_form"] = array(
			array('data' => t('Include'), 'colspan' => 'include'),
			array('data' => t('Path'), 'colspan' => 'path'),
			array('data' => t('Type'), 'colspan' => 'type'),
			array('data' => t('Media'), 'colspan' => 'media'),
			array('data' => t('Preprocess'), 'colspan' => 'preprocess'),
			array('data' => t('Weight'), 'field' => 'weight', 'sort' => 'asc'),
		);	
		
		$headers["theme_prepross_injector_list_node_configuration_form"] = array(
			array('data' => t('Node'), 'field' => 'nid', 'sort' => 'asc'),
		);
		
		return ($headers[$name_header]);
	}
	
	private function init_theme(){
		$themes 	= list_themes();
		$theme 		= variable_get('theme_default', null);
		// helper function to load CSS and JS files from [default_theme_name].info
		_init_theme($themes[$theme]);	
	}
	
	public function setNid($nid){
		$this->nid = $nid;
	}

	public function getNid(){
		return ($this->nid);
	}	

	public function getCss(){	
		return ($this->css);
	}

	public function getJs(){	
		return ($this->js);
	}	
}
