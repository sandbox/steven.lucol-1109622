<?php
// $Id$ 
/**
 * @file
 * Design pattern DAO
 */
 
 /**
 * Class JS
 */
class JS
{
	private $path;
	private $type;
	private $scope;
	private $defer;
	private $cache;
	private $preprocess;
	private $status;
	private $weight;
	private $gender;
	private $media;
	
	function __construct($path = NULL, $type = 'module', $scope = 'header', $defer = FALSE, $cache = TRUE, $preprocess = TRUE, $status = 'include', $weight = 0){
		$this->path = $path;
		$this->type = $type;
		$this->scope = $scope;
		$this->defer = $defer;
		$this->cache = $cache;
		$this->preprocess = $preprocess ? 1 : 0;	
		$this->status = $status;
		$this->weight = $weight;
		$this->gender = 'js';
	}
	
	public function setPath($path){
		$this->path = $path;
	}

	// getters
	public function getPath(){
		return ($this->path);
	}

	public function getType(){
		return ($this->type);
	}

	public function getScope(){
		return ($this->scope);
	}

	public function getDefer(){
		return ($this->defer);
	}

	public function getCache(){
		return ($this->cache);
	}	
	
	public function getMedia(){
		return ($this->media);
	}

	public function getPreprocess(){
		return ($this->preprocess);
	}

	public function getStatus(){
		return ($this->status);
	}

	public function getId(){
		return ($this->id);
	}
	
	public function getWeight(){
		return ($this->weight);
	}	

	public function getGender(){
		return ($this->gender);
	}	
}
