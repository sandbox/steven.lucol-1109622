<?php
// $Id$ 
/**
 * @file
 * Design pattern DAO
 */
 
 /**
 * Class CSS
 */
class CSS
{
	private $path;
	private $type;
	private $media;
	private $preprocess;
	private $status;
	private $weight;
	private $gender;
	
	function __construct($path = NULL, $type = 'module', $media = 'all', $preprocess = TRUE, $status = 'include', $weight = 0){
		$this->path = $path;
		$this->type = $type;
		$this->media = $media;
		$this->preprocess = $preprocess ? 1 : 0;	
		$this->status = $status;
		$this->weight = $weight;
		$this->gender = 'css';
	}
	
	public function setPath($path){
		$this->path = $path;
	}

	// getters
	public function getPath(){
		return ($this->path);
	}

	public function getType(){
		return ($this->type);
	}
	
	public function getMedia(){
		return ($this->media);
	}

	public function getPreprocess(){
		return ($this->preprocess);
	}

	public function getStatus(){
		return ($this->status);
	}

	public function getId(){
		return ($this->id);
	}
	
	public function getWeight(){
		return ($this->weight);
	}	

	public function getGender(){
		return ($this->gender);
	}	
}
