<?php
// $Id$ 
/**
 * @file
 * Design pattern DAO
 */

/**
 * Abstract class DAO
 */ 
abstract class DAO
{
	private $connect;
	
	abstract protected function find($id = null);
	abstract protected function create($obj);
	abstract protected function update($obj);
	abstract protected function delete($id = null);

	public function getConnect(){	
		return ($this->connect);
	}	
	
	public function setConnect($connect){
		$this->connect = connect;
	}		
}
