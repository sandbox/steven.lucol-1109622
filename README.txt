$Id$ 
********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Prepross Injector
Author: Steven Lucol
Drupal: 6
********************************************************************


DESCRIPTION:

This module allows you to setting inclusion of JS and CSS files 
in nodes.

For example, you can need to exclude few JS or CSS files in a 
particular node. Prepross Injector allow you to handle this by selecting
which files you want to include or exlude in this node.

Each node have his own setting.

********************************************************************
INSTALLATION:


1. Place the entire actions directory into your Drupal modules
   directory (normally sites/all/modules or modules).

2. Enable the action module by navigating to:

     Administer > Site building > Modules

3. If you want anyone besides the administrative user to be able
   to configure actions (usually a bad idea), they must be given
   the "administer actions" access permission:
   
     Administer > User management > Access control

   When the module is enabled and the user has the "administer
   actions" permission, an "actions" menu should appear under 
   Administer > Site configuration in the menu system.

   
4. Go to :

	/admin/settings/prepross_injector

5. Start to manage files inclusion with Propross Injector !

********************************************************************